Django==4.2.4
aiohttp==3.8.5
aiosignal==1.3.1
asgiref==3.7.2
async-timeout==4.0.3
attrs==23.1.0
backports.zoneinfo==0.2.1
cachetools==5.3.1
certifi==2023.7.22
charset-normalizer==3.2.0
djangorestframework==3.14.0
frozenlist==1.4.0
idna==3.4
ipinfo==4.4.3
multidict==6.0.4
python-decouple==3.8
redis==5.0.0
requests==2.31.0
sqlparse==0.4.4
typing-extensions==4.7.1
urllib3==1.26.16
yarl==1.9.2
