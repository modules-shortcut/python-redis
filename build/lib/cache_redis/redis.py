import redis, json


class Redis:
    def __init__(self, host='localhost', port=6379, user=None, password=None, db=0, default_expired=60*60*24, **kwargs):
        self.host=host
        self.port=port
        if user and password:
            self.username=user
            self.password=password
        self.default_expired=default_expired
        self.db=db
            
        self.conn = self.__connect()
    
    def __connect(self):
        return redis.Redis(host=self.host, port=self.port, db=self.db)
    
    
    def set(self, key=None, data=None, ttl=0):
        
        if not key or not data:
            return False
        
        ttl = self.default_expired if ttl == 0 else ttl
        if self.conn.set(key, json.dumps({'data':data})):
            return self.conn.expire(key, ttl)
        
        return False
    
    def get(self, key=None):
        data = self.conn.get(key)
        if data:
            value = json.loads(data)
            return value['data']
        return None
    
    def delete(self, key):
        return self.conn.delete(key)
    def flush(self):
        return self.conn.flushall()
    