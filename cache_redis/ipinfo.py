import ipinfo
from cache_redis.redis import Redis
from decouple import config

HTTP_X_FORWARDED_FOR = "HTTP_X_FORWARDED_FOR"
REMOTE_ADDR = "REMOTE_ADDR"

redis_host    = config("REDIS_HOST", default='192.168.69.20')
redis_port    = config("REDIS_PORT", default=6379)
redis_db      = config("REDIS_NAME", default=0)

ipinfo_token  = config("IPINFO_TOKEN", "8885854e240b9b")

try:
    from django.conf import settings
    redis_host = settings.REDIS_SETTING['HOST']
    redis_port = settings.REDIS_SETTING['PORT']
    redis_db   = settings.REDIS_SETTING['NAME']
    
    ipinfo_token = settings.IPINFO_TOKEN

except Exception:
    pass


redis = Redis(host=redis_host, port=redis_port, db=redis_db)
ttl = 60*60*24*30

class IpInfo:
    
    def __init__(self, token=None):
        self.handler = ipinfo.getHandler(token)
    
    
    def detail(self, ip=None):
        if not ip:
            return None
        
        detail = redis.get(ip)
        if not detail:
            try:
                detail = self.handler.getDetails(ip)
                detail =  {
                    'ip': detail.ip,
                    'latitude': detail.latitude,
                    'longitude': detail.longitude,
                    'city': detail.city,
                    'country_name': detail.country_name
                }
            except Exception as ex:
                detail =  {
                    'ip': ip,
                    'latitude': 0,
                    'longitude': 0,
                    'city': '-',
                    'country_name': '-'
                }
                
            redis.set(ip,detail, ttl)
        return detail
    

ip = IpInfo(ipinfo_token)

class IpInfoMiddleware:
    def __init__(self, get_response):
        self.get_response = get_response

    def __call__(self, request):
        
        if '/api/' not in request.path:
            response = self.get_response(request)
            return response
        
        request.ipinfo = ip.detail(self.get_ip(request)) 
        response = self.get_response(request)
        return response
    
    def get_ip(self, request):
        """Determine what IP to query for depending on whether we are behind a reverse proxy or not."""
        x_forwarded_for = request.META.get(HTTP_X_FORWARDED_FOR)
        if x_forwarded_for:
            return x_forwarded_for.split(",")[0]
        return request.META.get(REMOTE_ADDR)