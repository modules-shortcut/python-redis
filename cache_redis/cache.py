from django.core.cache import cache
from rest_framework.response import Response
import os

class Cache:
    def __init__(self, base=None, request=None, response=None, model=None, timeout=60, *args, **kwargs):
        self.base = base
        self.request = request
        self.response_data = response  # Rename this to avoid conflicts
        self.model = 'cache' if model is None else model
        self.timeout = timeout
        self.args = args
        self.kwargs = kwargs
    
    def __get_key(self):
        user_id = self.request.user.id if self.request.user.is_authenticated else 'anonymous'
        reload_cookie = self.request.COOKIES.get('reload_cache', None)
        reload_cookie = reload_cookie is not None and self.model == reload_cookie
        
        reload_param  = self.request.GET.get('reload', 'false')
        reload_param  = reload_param is not None and reload_param == 'true'
        
        return f"{self.model}_{user_id}_{self.request.build_absolute_uri()}", reload_cookie or reload_param

    def response(self):  # Rename this method
        key, check = self.__get_key()
        
        # DELETE BEFORE GET
        if check:
            try:
                cache.delete(key)
            except:
                pass
            
        # GET DATA FROM CACHE
        try:
            data = cache.get(key)
            if data is not None:
                return Response(data=data)
        except:
            pass
        
        # GET DATA FROM DATABASE
        res = self.base.list(self.request, *self.args, **self.kwargs) if self.response_data is None else self.response_data
        if res.status_code == 200:
            cache.set(key, res.data, timeout=self.timeout)
        return res

    def reload(self):
        if self.response_data is None:
            res = self.base.create(self.request, *self.args, **self.kwargs) if self.request.method == 'POST' else self.base.update(self.request, *self.args, **self.kwargs) if self.request.method == 'PUT' else self.base.partial_update(self.request, *self.args, **self.kwargs)
        else:
            res = self.response_data
        res.set_cookie(
            key='reload_cache',
            value=self.model,
            max_age=self.timeout,
            secure=True,
            httponly=True
        )
        return res