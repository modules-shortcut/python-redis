from setuptools import setup, find_packages

# read requirements files
with open('requirements.txt', 'r') as f:
    requirements = f.read().splitlines()
    
setup(
    name='cache_redis',
    version='1.0',
    author='dayat@rumahlogic.com',
    description='cache redis',
    packages=find_packages(),
    install_requires=requirements,
)